#!/bin/bash

UA="Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"
URL="https://raw.githubusercontent.com/wcb1969/iptv/main/%E6%97%A5%E6%9C%AC.txt"
m3u_file=wcb1969_iptv.m3u

echo "github: https://github.com/wcb1969/iptv"

echo "#EXTM3U" >$m3u_file
curl -sg "$URL"|sed -r \
  "s|(.*),(.*)|#EXTINF:-1 group-title=\"anime\" user-agent=\"$UA\",\1\n\2|g" \
  >>$m3u_file
